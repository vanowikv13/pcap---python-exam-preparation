x = input()
y = input()

# print(x / y)  # error expect to be one of two int
print(int(x) / int(y))  # 0.5
print(int(x) // int(y))  # 0

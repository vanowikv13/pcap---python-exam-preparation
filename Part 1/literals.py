x = 11111111
# here is the same number but write more readable
y = 11_111_111
print(x)
print(y)

# octal we can write as (zero - o)
print(0o123)  # print convert to decimal value
# hex we can write as (zero - x)
print(0x123)  # print convert to decimal value
# binary number we can write as (zero - b)
print(0b1101011)

# numbers withe scientific notation
print(3e8)  # 3 * 10^8 //convert to float number
print(4.78e2)  # 4.78 * 10^2
print(6.62607E-34)  # Planck's constant

print("I like \"Monty Python\"")
print('I like "Monty Python"')

# read two numbers
number1 = int(input("Enter a first number: "))
number2 = int(input("Enter a second number: "))
maxNum = number2

if number1 > number2:
    maxNum = number2

print("the largest number is ", maxNum)

# from, until without it, increment value
for i in range(0, 10, 2):
    print(i)

# else value is executed when statement is not true
i = 1
while i < 5:
    print(i)
    i += 1
else:
    print("else:", i)

for i in range(5):
    print(i)
else:
    print("else: ", i)  # will be print 4 cause loop doesn't increment to 5

i = 111
# to variable i will be not anything assigned
for i in range(2, 1):
    print(i)
else:
    print("else: ", i)

# not operator only can change condition from true to false and from false to true
print(not i)

# & - (ampersand) bitwise conjunction
# | (bar) bitwise disjunction
# ~ (tilde) bitwise negation
# ^ (caret) bitwise exclusive or (xor)

# this only apply to a int number
Var = 17  # 10001
VarRight = Var >> 1  # 1000
VarLeft = Var << 2  # 1000100
print(Var, VarLeft, VarRight)

numbers = [1, 2, 3, 4, 5]
del numbers[4]
print(len(numbers))
print(numbers[-1])  # write last number
print(numbers[-4])  # write first number
numbers.append(12)
numbers.insert(1, 2)

# swap in python
var1 = 1
var2 = 0
var1, var2 = var2, var1
print(var1, var2)

# the name of an ordinary variable is the name of its content;
# the name of a list is the name of a memory location where the list is stored.
# it will just copy the name of the array, not its contents
list1 = [1]
list2 = list1
list1[0] = 2
print(list2)   # so it means that output will be [2]

# to copy just list values not it's address we need to use operator slice
list3 = list1[:]
list1[0] = 3
print(list3)   # [2]

# slice operator [start : end -1]
list1 = [1, 2, 3, 4, 5]
print(list1[1:3])  # [2, 3]
print(list1[1:-1])  # [2, 3, 4]
print(list1[-1:1])  # []
# list  is empty: if the first element is laying further then end the slice will be empty

print(list1[:3])  # [1, 2] -> list1[0:3]
print(list1[3:])  # [4, 5] -> list1[3:4]

del list1[1:3]
print(list1)  # [1, 4, 5]
del list1[:]
print(list1)  # []
del list1
# print(list1)  # will cause runtime error cause del list1 delete variable not it's value

list1 = [1, 2, 3, 4, 5]
print(2 in list1)  # true
print(6 not in list1)  # true

print([i for i in range(3)])   # [0, 1, 2]
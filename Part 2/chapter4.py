import math

for name in dir(math):
    print(name, end="\t")

x = 1.4
y = 2.6

print(math.floor(x), math.floor(y))  # 1 2
print(math.floor(-x), math.floor(-y))  # -2 -3

print(math.ceil(x), math.ceil(y))  # 2 3
print(math.ceil(-x), math.ceil(-y))  # -1 -2

print(math.trunc(x), math.trunc(y))  # 1 2
print(math.trunc(-x), math.trunc(-y))  # -1 -2

from random import random, seed, randrange, randint

seed()  # set the seed with current time (seed(i) - > set the seed with intiger time)

for i in range(5):
    print(random())

# randrange(beg, end, step) -> end and step are not need
# randit(left, right)

print(randrange(1, 10, 2))
print(randint(1, 10))

from platform import platform, machine, processor, system

print(platform())  # platform(aliased=False, terse=False)
print(platform(1))  # when set to True (or any non-zero value) it may cause the function to present the alternative
# underlying layer names instead of the common ones;
print(platform(0, 1))  # when set to True (or any non-zero value) it may convince the function to present a briefer
# form of the result (if possible)

print(machine())
print(processor())
print(system())

import module

def func():
    raise ZeroDivisionError

try:
    x = input()
    assert x>=0 # make a condition if it's not true it will throw a error AssertionError
    func()
except ZeroDivisionError:
    print("Can't divide by 0")
except:
    print("You can't do that")

multiLine = '''
    Some string
''' # or with """ """

print(multiLine)

greekAlfa = chr(945)

print(greekAlfa)
print(ord(greekAlfa))

text = "132123413"
print('2' in text)


try:
    text.index('a')
except ValueError:
    print("Value error")

print(list(text))   # ['1', '3', '2', '1', '2', '3', '4', '1', '3']

print(text.count('1'))
print(text.find('132')) # find(value, indexWhereStart, indexWhereEnd)
print(text.center(6, '*'))
print(text.endswith('2'))
print('aNoego'.capitalize())
print(','.join(['123', 'abc', '(*&']))
print('Hello World'.replace('World', 'You', 1)) # the last number is to limit number of replacement
print('Hello World'.split())
print('Hello World'.swapcase())
print('hello world'.title())    # Hello World
print('1'.isdigit())
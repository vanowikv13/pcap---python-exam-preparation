class Stack:
    Counter = 0 # static member
    def __init__(self):
        Stack.Counter += 1
        self.__stack = []

    def push(self, val):
        self.__stack.append(val)

    def pop(self):
        val = self.__stack[-1]
        del self.__stack[-1]
        return val


# subClass of Stack
class AddingStack(Stack):
    def __init__(self):
        Stack.__init__(self)
        self.sum = 0

    def getSum(self):
        return self.sum

    def push(self, val):
        self.sum += val
        Stack.push(self, val)

    def pop(self):
        val = Stack.pop(self)
        self.sum -= val
        return val

    def __hiden(self):
        print('hiden')



stack = Stack()
addingStack = AddingStack()

addingStack.push(13)
addingStack.getSum()
addingStack.pop()


stack.push(12)
print(stack.pop())

print(hasattr(addingStack, 'prop'))

def reciprocal(n):
    try:
        n = 1 / n
    except ZeroDivisionError:
        print("Divison Failed")
        n = None
    else:
        print("everything went fine")
    finally:
        print("It's time to say goodbye")
        return n

print(reciprocal(2))
print(reciprocal(0))

def fun(n):
    for i in range(n):
        yield i

for v in fun(5):
    print(v)

print(list(fun(5)))

# lambdas
two = lambda : 2
sqr = lambda  x : x * x
pwr = lambda  x,y : x ** y

print(two())
print(sqr(two()))


# reading files
# + - means update

try:
    stream = open('file.txt', 'rt')
    stream.close()
except Exception as e:
    print("Open failed:", e)

d = { 1:0, 2:1, 3:2, 0:1 }
x = 0
for y in range(len(d)):
    x = d[x]
print(x)